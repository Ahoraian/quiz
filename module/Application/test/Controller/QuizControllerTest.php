<?php

/**
 * @see       https://github.com/laminas/laminas-mvc-skeleton for the canonical source repository
 * @copyright https://github.com/laminas/laminas-mvc-skeleton/blob/master/COPYRIGHT.md
 * @license   https://github.com/laminas/laminas-mvc-skeleton/blob/master/LICENSE.md New BSD License
 */

declare(strict_types=1);

namespace ApplicationTest\Controller;

use Application\Controller\IndexController;
use Application\Controller\QuizController;
use Laminas\Stdlib\ArrayUtils;
use Laminas\Test\PHPUnit\Controller\AbstractHttpControllerTestCase;

class QuizControllerTest extends AbstractHttpControllerTestCase
{
    public function setUp(): void
    {
        // The module configuration should still be applicable for tests.
        // You can override configuration here with test case specific values,
        // such as sample view templates, path stacks, module_listener_options,
        // etc.
        $configOverrides = [];

        $this->setApplicationConfig(ArrayUtils::merge(
            include __DIR__ . '/../../../../config/application.config.php',
            $configOverrides
        ));

        parent::setUp();
    }

    public function loginCredentialsProvider()
    {
        return [
            'farzin2008@gmail.com',
            'secret'
        ];
    }

    public function testIndexActionCanBeAccessed()
    {
        $this->dispatch('/login', 'POST', $this->loginCredentialsProvider());

        $this->dispatch('/quiz/index', 'GET');
        $this->assertResponseStatusCode(200);

        $this->assertModuleName('application');
        $this->assertControllerName(QuizController::class); // as specified in router's controller name alias
        $this->assertControllerClass('QuizController');
        $this->assertMatchedRouteName('quiz');
    }

    public function testCreateActionCanBeAccessed()
    {
        $this->dispatch('/login', 'POST', $this->loginCredentialsProvider());

        $this->dispatch('/quiz/create', 'GET');
        $this->assertResponseStatusCode(200);

        $this->assertModuleName('application');
        $this->assertControllerName(QuizController::class); // as specified in router's controller name alias
        $this->assertControllerClass('QuizController');
        $this->assertMatchedRouteName('quiz');
    }

    public function testPostCreateAction()
    {
        $this->dispatch('/login', 'POST', $this->loginCredentialsProvider());

        $timeout = rand(1, 3);
        $this->dispatch('/quiz/create', 'POST', [
            'title' => 'unit test ' . rand(100, 1000),
            'category_id' => rand(1, 5),
            'timeout' => $timeout > 1 ? ' Days' : ' Day',
            'question' => 'Where are you from [' . rand(100, 1000) . ']?',
            'answers' => ['test 1', 'test 2']
        ]);
        $this->assertResponseStatusCode(200);
    }
}
