# Simple Quiz Application (Laminas)

## Introduction

This is a simple quiz application with admin section to manage all created quizzes by users.

## Installation

install data/sql/install.sql into mysql then create user and password and set in configs.

## Test
```bash
./vendor/bin/phpunit
```